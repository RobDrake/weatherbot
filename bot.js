/* Notes:
 * http://api.wunderground.com/api//df369542a9de8eea/conditions/q/64759.json
 */
var Irc = require('irc'),
	Sleep = require('sleep'),
	config = require('./config/botconfig.json'),
	bluebird = require('bluebird'),
	bhttp = require('bhttp');



var main = function() {
	var bot;

	var say = function(who, output) {
		if( !Array.isArray( output )) {
			bot.say(who, output);
		} else if(output.length == 1) {
			bot.say(who, output[0]);
		} else 	{
			output.forEach(function(message) {
				bot.say(who, message);
				Sleep.msleep(700);
			});
		}
	};

	bot = new Irc.Client(config.irc.server, config.irc.botName, {
		channels: config.irc.channels, debug: true, floodProtection: true
	});

	bot.addListener('motd', function( motd ) {
		bot.say('nickserv', 'identify ' + config.irc.nickPass);
	});

	bot.addListener('error', function(message) {
		console.log(message);
	});

	bot.addListener('pm', function( from, message) {
		if( from != config.irc.botOwner) {
			console.log('pm from: ' + from);
			bot.say(from, 'leave me alone');
			return;
		} else {
			var words = message.toLowerCase();
			words = message.split(' ');
			if(words[0] == '!quit') {
				bot.disconnect('Yes Boss!');
				process.exit();
			}
		}
	});

	bot.addListener('join' + config.irc.channels[0], function(nick, message) {
		if(nick == config.irc.botName) {
			bot.addListener('message' + config.irc.channels[0], channelHandler);
		}
	});

	var channelHandler = function(nick, text, message) {
		var output = [];
		var words = text.toLowerCase();
		words = words.split(' ');
		
		if(words[0] == '.weather') {
			var countryCode = 'US';
			var postalCode = '90120';
			if(words.lentgh = 1) {
			}
			if(words.length == 2) {
				postalCode = words[1];
			} else if(words.length == 3) {
				countryCode = words[1];
				postalCode = words[2];
			} else {
				return;
			}
			/*
			http.get({
				host: 'api.wunderground.com',
				path: '/api//' + config.weather.access_key + '/conditions/q/' + countryCode + '/'
					+ postalCode + '.json'
			}, function(response) {
				var body = '';
				response.on('data', function(d) {
					body += d;
				});
				response.on('end', function() {
					var parsed = JSON.parse(body);

					if( parsed.response.error) {
						output[0] = 'Error someplace';
						say(config.irc.channels[0], output);
						help(config.irc.channels[0]);
					   console.log('Error found');
					   return;
					} else if(parsed.current_observation) {
						output[0] =  parsed.current_observation.weather + ' | ' +
							'Temp: ' + parsed.current_observation.temp_f + 'F' +
							' | ' + 'Wind: ' + parsed.current_observation.wind_string +
							' | ' + parsed.current_observation.observation_time;
						say(config.irc.channels[0], output);
					} else {
						console.log('other error');
						help(config.irc.channels[0]);
					}
				});
			});
			return;
		}
		*/
		Promise.try( function() {
			return bhttp.get( `http://api.wunderground.com/api//${config.weather.access_key}/conditions/q/${countryCode}
				/${postalCode}.json`);
			}).then( function(response) {
				if(!response) {
					return;
				}
				response = response.req.res.body;
				output = [
					response.current_observation.weather,
					`Temp: ${response.current_observation.temp_f}F`,
					`Wind: ${response.current_observation.wind_string}`,
					response.current_observation.observation_time
				];
				say(output.join(' | '));
			
		}).catch( function(e) {
			console.log(e);
		});
	};

	var help = function( channel ) {
		var output = [];
		output[0] = '.help';
		output[1] = '.weather [country code] [postal code]';
		output[2] = '    where [country code] defaultsto US';
		output[3] = '    and [postal code] defaults to 90210';
		say(channel, output);
	};
};

main();
